import dayjs from "dayjs";

const formulario = document.getElementById("ffechas");

formulario.addEventListener("submit", (e) => {
  let data = new FormData(formulario as HTMLFormElement);

  let fecha1 = dayjs(data.get("fecha1").toString());
  let fecha2 = dayjs(data.get("fecha2").toString());

  let diff = fecha2.diff(fecha1, "d");

  document.getElementById("dias").innerHTML = diff.toString();

  e.preventDefault();
  return false;
});
